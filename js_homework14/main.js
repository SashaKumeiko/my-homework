let $tabs=$('.tabs');

$(".tabs-title:first-child").addClass("active");
$(".paragraph:first-child").addClass("paragraph-active");

$tabs.on('click', handler);

function handler(event) {
    if (event.target.dataset.hasOwnProperty('tabLink')) {
        const linkValue = event.target.dataset.tabLink;

        /* Деактивируем прошлые табы и контент */
        $('.active').removeClass('active');
        $('.paragraph-active').removeClass('paragraph-active');

        /* Активируем текущие */
      
        event.target.classList.add('active');
   $(`[data-tab-name='${linkValue}']`)
.addClass('paragraph-active')
    }
}

