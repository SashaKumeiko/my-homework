axios.get("https://swapi.co/api/films/").then(response => {
  let films = response.data.results;
  console.log(films.length);
  let filmsIterator = 1;
  let i = 1;
  // for (let i = 1; i <= films.length; i++) {

  // }
  films.forEach(el => {
    let filmDiv = document.createElement("div");
    filmDiv.classList.add(`film${i++}`);
    filmDiv.classList.add("film");

    document.querySelector(".films").append(filmDiv);

    let film = document.createElement("div");
    film.innerHTML =
      el.title +
      "\n" +
      `number of episodes:` +
      el.episode_id +
      "\n" +
      el.opening_crawl;
    let div = document.querySelector(`.film${filmsIterator++}`);
    div.append(film);

    let characters = document.createElement("div");
    let urls = el.characters;
    let str = "";
    const promises = [];
    console.log(urls);
    for (const url of urls) {
      //   console.log(url);
      promises.push(
        axios.get(url).then(response => {
          // console.log(response.data)
          str += response.data.name;
          // console.log(str)
          characters.innerHTML = str;
        })
      );
    }

    Promise.all(promises).then(resolve => {
      div.append(characters);
    });
  });
});
