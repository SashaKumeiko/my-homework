const wrapper = document.querySelector('.images-wrapper');
let isActive=false;
function timeint() {
    isActive=true;
    if (document.querySelector('.active').nextElementSibling === null) {
        document.querySelector('.image-to-show:first-child').classList.add('active')
        document.querySelector('.image-to-show:last-child').classList.remove('active')
    } else {
        document.querySelector('.active').nextElementSibling.classList.add('active');
        document.querySelector('.active').classList.remove('active');
    }
}

let interval = setInterval(timeint, 10000);
const div = document.createElement('div');
div.innerHTML = `<button class="stop">stop</button><button class="continue">continue</button>`;
document.body.append(div);

function funcStop() {
    isActive=false;
    clearInterval(interval);
}

const stop = document.querySelector('.stop');
stop.addEventListener('click', funcStop);
const cont = document.querySelector('.continue');

function funcContinue() {
    if(isActive===false) {
        interval = setInterval(timeint, 10000)
    }
}

cont.addEventListener('click', funcContinue);
