
const tabs = document.querySelector('.tabs');
const tabContent = document.querySelector('.tabs-content');

tabs.addEventListener('click', handler);

function handler(event) {
    const element = event.target;

    if (element.dataset.hasOwnProperty('tabLink')) {
        const linkValue = element.dataset.tabLink;

        /* Деактивируем прошлые табы и контент */
        tabs.querySelector('.active').classList.remove('active');
        tabContent.querySelector('.par-active').classList.remove('par-active');


        const paragraphToActivate = tabContent.querySelector(`[data-tab-name='${linkValue}']`);

        /* Активируем текущие */
        element.classList.add('active');
        paragraphToActivate.classList.add('par-active')
    }
}

