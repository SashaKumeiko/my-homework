class Columns {
  constructor() {
    this.cardID = 1;
    this.columnID = 1;
    this.addColumn();
    document.querySelector(".column-adder").addEventListener("click", () => {
      this.addColumn();
    });
  }

  addColumn() {
    let newColumn = document.createElement("div");
    newColumn.classList.add("column");

    newColumn.setAttribute("id", "list" + this.columnID);
    console.log(this.columnID);
    this.columnID++;

    newColumn.addEventListener("dragover", function(event) {
      event.preventDefault();
    });

    newColumn.innerHTML = `<button class="button-add-card" type="button">Add card</button>
    <button class="button-alphabet" type="button">Alphabeic order</button><div class="card" draggable="true"><textarea name="" id="" cols="50" rows="4"></textarea></div>`;
    let columnContainer = document.querySelector(".column-container");
    columnContainer.appendChild(newColumn);
    newColumn.lastChild.setAttribute("id", "card" + this.cardID++);
    newColumn.lastChild.addEventListener("drop", this.drop);
    newColumn.lastChild.addEventListener("dragstart", function(ev) {
      ev.dataTransfer.setData("text/plain", ev.target.id);
    });
    newColumn.firstChild.addEventListener("click", () => {
      this.addCard(event);
    });

    newColumn
      .querySelector(".button-alphabet")
      .addEventListener("click", this.sortAZ);
  }
  sortAZ(event) {
    let list = event.target.parentElement;
    let cardNodes = list.querySelectorAll(".card");
    let ArrayOfValues = [];
    cardNodes.forEach(el => {
      ArrayOfValues.push(el.firstChild.value);
    });
    console.log(ArrayOfValues);
    let sortedArray = ArrayOfValues.sort().reverse();
    console.log(sortedArray);
    let iteratorOnSorted = 0;
    cardNodes.forEach(el => {
      el.firstChild.value = sortedArray[iteratorOnSorted++];
    });
  }
  addCard(event) {
    let newCard = document.createElement("div");
    newCard.classList.add("card");
    newCard.setAttribute("id", "card" + this.cardID);
    this.cardID++;
    newCard.addEventListener("dragstart", function dragStart(ev) {
      ev.dataTransfer.setData("text/plain", ev.target.id);
    });
    newCard.setAttribute("draggable", "true");

    newCard.addEventListener("drop", this.drop);

    newCard.innerHTML = `<textarea name="" id="" cols="50" rows="4"></textarea>`;
    let column = event.target.parentElement;
    column.insertAdjacentElement("beforeend", newCard);
  }
  drop(ev) {
    ev.preventDefault();
    let sourceId = ev.dataTransfer.getData("text/plain");
    let sourceIdEl = document.getElementById(sourceId);
    let sourceIdParentEl = sourceIdEl.parentElement;
    console.log(ev.target.id);

    let targetEl = document.getElementById(ev.currentTarget.id);
    let targetParentEl = targetEl.parentElement;
    console.log(targetParentEl);

    if (targetParentEl.id !== sourceIdParentEl.id) {
      if (targetEl.className === sourceIdEl.className) {
        targetParentEl.appendChild(sourceIdEl);
      } else {
        targetEl.appendChild(sourceIdEl);
      }
    } else {
      let holder = targetEl;

      let holderVal = holder.firstChild.value;
      targetEl.firstChild.value = sourceIdEl.firstChild.value;
      sourceIdEl.firstChild.value = holderVal;
      holderVal = "";
    }
  }
}
const A = new Columns();
