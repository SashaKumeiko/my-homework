let menuIcon = document.querySelector('.menu__icon');
menuIcon.addEventListener('click', handler);

function handler(t) {
    let x = document.querySelector(".dropdown-content");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
    let menuIcon = document.querySelector('.menu__icon');
    menuIcon.classList.toggle("change");
}
