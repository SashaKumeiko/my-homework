
let gulp = require('gulp');
let sass = require('gulp-sass');
let sync = require('browser-sync')
    // , modifyCssUrls = require('gulp-modify-css-urls');

gulp.task('sass', function () {
  return gulp.src(['./src/scss/**/*.scss', './src/scss/**/*.sass'])
    .pipe(sass({}))
    .pipe(gulp.dest('./dist/css'))
    .pipe(sync.reload({stream: true}))
});
gulp.task('html', function(){
  return gulp.src('./dist/*html')
  .pipe(sync.reload({stream:true}))
})

// gulp.task('modifyUrls', function () {
//     return gulp.src('../dist/img/menu-symbol.png')
//         .pipe(modifyCssUrls({
//             modify: function (url) {
//                 return url.replace('../dist', '');
//             },
//         }))
//         .pipe(gulp.dest('./'));
// });
// gulp.task('img', function(){
//     return gulp.src('src/img/*.*')
//         .pipe(gulp.dest('dist/img'))
// });

gulp.task('watch', function () {
  	gulp.watch(['./src/scss/**/*.scss', './src/scss/**/*.sass'], gulp.parallel('sass'));
	  gulp.watch('./dist/*.html', gulp.parallel('html'));
	  // gulp.watch('src/img/*.*', gulp.parallel('img'))
});

gulp.task('sync', function () {
  sync.init({
    server: {
      baseDir:'./dist'
    }
  })
});



gulp.task('default', gulp.parallel('sync', 'watch'));
