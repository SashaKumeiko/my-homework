let buttons=document.querySelectorAll('.btn');

let previousKey;
document.addEventListener('keydown', handler);

function handler(event) {
    for(let i=0;buttons.length-1;i++)
    {

        if (previousKey !== undefined) {
            previousKey.style.backgroundColor = '#33333a';
        }

        if(buttons[i].innerText.toUpperCase() === event.key.toUpperCase()){
            buttons[i].style.backgroundColor='blue';
            previousKey=buttons[i];
            break;
        }
    }
}

// const enter = document.querySelector('.btn:nth-child(1)');
// const s = document.querySelector('.btn:nth-child(2)');
// const e = document.querySelector('.btn:nth-child(3)');
// const o = document.querySelector('.btn:nth-child(4)');
// const n = document.querySelector('.btn:nth-child(5)');
// const l = document.querySelector('.btn:nth-child(6)');
// const z = document.querySelector('.btn:nth-child(7)');
//
// let previousKey;
//
// document.addEventListener('keydown', function (event) {
//     if (previousKey !== undefined) {
//         previousKey.style.backgroundColor = '#33333a';
//     }
//
//     switch (event.code) {
//         case 'Enter':
//             enter.style.backgroundColor = 'blue';
//             previousKey = enter;
//             // previousKey=enter;
//             break;
//         case 'KeyS':
//             s.style.backgroundColor = 'blue';
//             previousKey = s;
//             break;
//         case 'KeyE':
//             e.style.backgroundColor = 'blue';
//             previousKey = e;
//             break;
//         case 'KeyO':
//             o.style.backgroundColor = 'blue';
//             previousKey = o;
//             break;
//         case 'KeyN':
//             n.style.backgroundColor = 'blue';
//             previousKey = n;
//             break;
//         case 'KeyL':
//             l.style.backgroundColor = 'blue';
//             previousKey = l;
//             break;
//         case 'KeyZ':
//             z.style.backgroundColor = 'blue';
//             previousKey = z;
//             break;
//     }
// });