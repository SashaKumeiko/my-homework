$(window).scroll(onScroll);

function onScroll(e) {
    const className = 'btn-scroll-top';
    if (window.scrollY > window.innerHeight) {
        if (!$(`.${className}`).length) {
            $('<button>Go UP</button>')
                .css({
                    position: 'fixed',
                    right: 10,
                    bottom: 10
                })
                .addClass(className)
                .click(scrollTop)
                .appendTo(document.body);
        }
    } else {
        $(`.${className}`).remove();
    }
}

function scrollTop() {
    $("html, body").animate({scrollTop: 0}, 1000);
}

$(`<button>Show/hide button</button>`).appendTo('.flex-part').click(function () {
    $(".posts").slideToggle("slow");})


$('.header-flex').on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});