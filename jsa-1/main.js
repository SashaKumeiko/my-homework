function HamburgerException(x) {
  this.message = x;
  this.name = HamburgerException;
}
function Hamburger(size, stuffing) {
  if (!size || !stuffing) {
    throw new HamburgerException("enter size and stuffing!");
  }
  this.size = size;
  this.stuffing = stuffing;
  this.topping = [];
}
Hamburger.SIZE_SMALL = {
  type: "size",
  param: "SIZE_SMALL",
  price: 50,
  calories: 20
};
Hamburger.SIZE_LARGE = {
  type: "size",
  param: "SIZE_LARGE",
  price: 100,
  calories: 40
};
Hamburger.STUFFING_CHEESE = {
  type: "stuffing",
  param: "STUFFING_CHEESE",
  price: 10,
  calories: 20
};
Hamburger.STUFFING_SALAD = {
  type: "stuffing",
  param: "STUFFING_SALAD",
  price: 20,
  calories: 5
};
Hamburger.STUFFING_POTATOS = {
  type: "stuffing",
  param: "STUFFING_POTATOS",
  price: 15,
  calories: 10
};
Hamburger.TOPPING_MAYO = {
  type: "topping",
  param: "TOPPING_MAYO",
  price: 20,
  calories: 5
};
Hamburger.TOPPING_SPICE = {
  type: "topping",
  param: "TOPPING_SPICE",
  price: 20,
  calories: 0
};

Hamburger.prototype.addTopping = function(top) {
  if (!top) {
    throw new HamburgerException("topping is required");
  }
  if (top.type !== "topping") {
    throw new HamburgerException("it is not a topping");
  }
  if (this.topping.type && this.topping.type.includes(top.type)) {
    throw new HamburgerException(
      "Dublicate topping! It has been already added"
    );
  }
  this.topping.push(top);
};

Hamburger.prototype.removeTopping = function(top) {
  console.log("topping param to delete", top.param);
  if (!top) {
    throw new HamburgerException("select proper topping to remove");
  }
  if (!this.topping.length) {
    throw new HamburgerException("there wasn't such topping in hamburger");
  }
  if (!this.topping.includes(top)) {
    throw new HamburgerException("there is no more toppings to remove");
  }
  this.topping.splice(this.topping.indexOf(top), 1);
};

Hamburger.prototype.getToppings = function() {
  if (this.topping.length === 2) {
    console.log("toppings", this.topping);
  }
  if (this.topping.length === 1) {
    console.log("topping", this.topping);
  }
  if (this.topping.length === 0) {
    console.log("no toppings", []);
  }
};

Hamburger.prototype.getSize = function() {
  console.log("size is", this.size);
};
Hamburger.prototype.getStuffing = function() {
  console.log("stuffing is", this.stuffing);
};
Hamburger.prototype.calculatePrice = function() {
  var toppingsPrice = 0;
  for (var element of this.topping) {
    if (element.price) {
      toppingsPrice += element.price;
    }
  }

  var price = this.size.price + this.stuffing.price + toppingsPrice;

  console.log("price is", price);
};
Hamburger.prototype.calculateCalories = function() {
  var toppingsCalories = 0;
  for (var element of this.topping) {
    if (element.calories) {
      toppingsCalories += element.calories;
    }
  }

  var calories = this.size.calories + this.stuffing.calories + toppingsCalories;

  console.log("calories:", calories);
};
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log("new object:", hamburger);

try {
  hamburger.addTopping(Hamburger.TOPPING_MAYyO);
} catch (e) {
  console.log(e, "catched error");
}

hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_SPICE);

hamburger.getToppings();
hamburger.getSize();
hamburger.getStuffing();
hamburger.calculatePrice();
hamburger.calculateCalories();
// hamburger.removeTopping(Hamburger.TOPPING_MAYO);
// console.log("topping that is left", hamburger.topping[0].param);
hamburger.getToppings();
// hamburger.removeTopping(Hamburger.TOPPING_SPICE);
// console.log("topping that is left", hamburger.topping);
hamburger.getToppings();

// hamburger.removeTopping(Hamburger.TOPPING_SPICE);
// console.log("topping that is left", hamburger.topping);
hamburger.getToppings();
