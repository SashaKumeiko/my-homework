function HamburgerException(x) {
  this.message = x;
  this.name = HamburgerException;
}

class Hamburger {
  constructor(size, stuffing) {
  

    if (!size || !stuffing) {
      throw new HamburgerException("enter size and stuffing!");
    }
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
  };
  static SIZE_SMALL = {
    type: "size",
    param: "SIZE_SMALL",
    price: 50,
    calories: 20
  };
  
  static SIZE_LARGE = {
    type: "size",
    param: "SIZE_LARGE",
    price: 100,
    calories: 40
  };
  static STUFFING_CHEESE = {
    type: "stuffing",
    param: "STUFFING_CHEESE",
    price: 10,
    calories: 20
  };
  static STUFFING_SALAD = {
    type: "stuffing",
    param: "STUFFING_SALAD",
    price: 20,
    calories: 5
  };
  static STUFFING_POTATOS = {
    type: "stuffing",
    param: "STUFFING_POTATOS",
    price: 15,
    calories: 10
  };
  static TOPPING_MAYO = {
    type: "topping",
    param: "TOPPING_MAYO",
    price: 20,
    calories: 5
  };

  static TOPPING_SPICE = {
    type: "topping",
    param: "TOPPING_SPICE",
    price: 20,
    calories: 0
  };
  set addTopping(top) {


    if (!top) {
      throw new HamburgerException("topping is required");
    }
    if (top.type !== "topping") {
      throw new HamburgerException("it is not a topping");
    }
    if (this.topping.type && this.topping.type.includes(top.type)) {
      throw new HamburgerException(
        "Dublicate topping! It has been already added"
      );
    }
    this.topping.push(top);
  }
  get getToppings() {
    if (this.topping) {
      if (this.topping.length) {
        return this.topping
      }

      if (!this.topping.length) {
        console.log("no toppings");
      }
    }
  }
  removeTopping(top) {
    console.log("topping param to delete", top.param);
    if (!top) {
      throw new HamburgerException("select proper topping to remove");
    }
    if (!this.topping.length) {
      throw new HamburgerException("there wasn't such topping in hamburger");
    }
    if (!this.topping.includes(top)) {
      throw new HamburgerException("there is no more toppings to remove");
    }
    this.topping.splice(this.topping.indexOf(top), 1);
  };
  get getSize() {
    return this.size;
  }
  get getStuffing() {
    return this.stuffing;
  }
  calculatePrice() {
    let toppingsPrice = 0;
    for (let element of this.topping) {
      if (element.price) {
        toppingsPrice += element.price;
      }
    }

    let price = this.size.price + this.stuffing.price + toppingsPrice;

    return price;
  }

  calculateCalories() {
    let toppingsCalories = 0;
    for (let element of this.topping) {
      if (element.calories) {
        toppingsCalories += element.calories;
      }
    }

    let calories =
      this.size.calories + this.stuffing.calories + toppingsCalories;

    return calories;
  }
}

const burger = new Hamburger(
  Hamburger.SIZE_SMALL,
  Hamburger.STUFFING_CHEESE
);
console.log("created hamburer", burger);
try{
  burger.addTopping = Hamburger.TOppppppppppppppPPING_MAYO;

}
catch(e){
  console.log(e, "catched error")
}
burger.addTopping = Hamburger.TOPPING_MAYO;
burger.addTopping = Hamburger.TOPPING_SPICE;

console.log("get toppings", burger.getToppings);
// burger.removeTopping(Hamburger.TOPPING_MAYO);
console.log("get toppings", burger.getToppings);
console.log("get size", burger.getSize);
console.log("get stuffing", burger.getStuffing);
console.log("price is", burger.calculatePrice());
console.log("calories:", burger.calculateCalories());
