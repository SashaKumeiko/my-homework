const request = new XMLHttpRequest();
request.open("GET", "https://swapi.co/api/films/");
request.responseType = "json";
request.onload = () => {
  console.log(request.response);
  let films = request.response.results;
  let filmsIterator = 1;

  films.forEach(el => {
    let film = document.createElement("div");
    film.innerHTML =
      el.title +
      "\n" +
      `number of episodes:` +
      el.episode_id +
      "\n" +
      el.opening_crawl;
    let div = document.querySelector(`.film${filmsIterator}`);
    let characters = document.createElement("div");
    let urls = el.characters;
    let counter = 1;
    let str='';
    for (const url of urls) {
      //   console.log(url);
      let cRequest = new XMLHttpRequest();
      cRequest.open("GET", url);
      cRequest.responseType = "json";
      cRequest.onload = () => {
        str+=cRequest.response.name
        counter++;
        if(counter ===urls.length){
 
            characters.innerHTML = "\n" + "Characters: \n"+ str
        }
      };
      cRequest.send();
    }

      div.append(film);
      div.append(characters);
      filmsIterator++;
  });
};
request.send();
