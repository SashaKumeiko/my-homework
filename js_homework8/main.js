let input = document.querySelector('.input');
input.addEventListener('focus', onFocus);
let div = document.createElement('div');
let span = document.createElement('span');

function onFocus(e) {
    div.innerHTML = ' '
    e.target.style.borderColor = 'green';
}

input.addEventListener('blur', onBlur);

function onBlur(e) {
    if (e.target.value < 0 || isNaN(e.target.value)) {
        e.target.style.borderColor = 'red';
        span.innerText = `Please enter correct price`;
        document.body.prepend(div);
        div.prepend(span);
    } else {
        let button = document.createElement('button');
        e.target.style.borderColor = '';
        console.log(e.target.value);
        button.innerText = 'X';
        span.innerHTML = `Текущая цена: ${e.target.value}`;
        document.body.prepend(div);
        div.prepend(span);
        div.append(button);
        e.target.style.color = 'green';
        button.addEventListener('click', onClick);

        function onClick(evt) {
            div.remove();
            input.value = ' ';

        }
    }
}