async function requestFunction() {
  let result = await axios.get("https://api.ipify.org/?format=json");
  console.log(result.data.ip);

  let place = await axios.get(
    `http://ip-api.com/json/${result.data.ip}?lang=ru&fields=continent,country,regionName,city,district`
  );
  let info = JSON.stringify(place.data);
  let infoDiv = document.createElement("div");
  infoDiv.innerHTML = `<div>${info}</div>`;
  document.querySelector(".container").append(infoDiv);
}
