let table = document.querySelector('.table');
let button = document.querySelector('.button');

function initTheme() {
    if (localStorage.getItem('theme') !== null) {
        table.setAttribute('data-theme', localStorage.getItem('theme'));
    }
}

document.addEventListener('DOMContentLoaded', initTheme);


function themeChanger() {
    if (table.dataset.hasOwnProperty('theme')) {
        table.removeAttribute('data-theme');
        localStorage.removeItem('theme')

    } else {
        table.setAttribute('data-theme', 'dark');
        localStorage.setItem('theme', 'dark')
    }
}

button.addEventListener('click', themeChanger);


