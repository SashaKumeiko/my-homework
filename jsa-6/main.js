document.addEventListener("DOMContentLoaded", function() {
  Posts.getData();
});

class Posts {
  constructor(u, p) {
    this.users = u;
    this.posts = p;
  this.counterOfNewPosts = 101;

  }

  static getData() {
    let users;
    let posts;
    fetch("https://jsonplaceholder.typicode.com/users")
      .then(response => response.json())
      .then(json => {
        users = json;
        fetch("https://jsonplaceholder.typicode.com/posts")
          .then(response => response.json())
          .then(json => {
            posts = json;
            let A = new Posts(users, posts);
            A.createPosts();
          });
      });
  }
  createPosts() {
    console.log(this.users, this.posts);
    let container = document.querySelector(".container");
    container.innerHTML = `<button type="button" class="button-add-post button">add post</button>`;
    container
      .querySelector(".button-add-post")
      .addEventListener("click", this.addPost.bind(this));

    this.posts.forEach(el => {
      const card = document.createElement("div");
      card.classList.add("card");
      card.setAttribute("data-id", `${el.id}`);
      card.setAttribute("data-user", `${el.userId}`);

      const post = document.createElement("div");
      post.classList.add("post");
      post.innerHTML = `<div class="title">${el.title}</div>
      <div class="text">${el.body}
      </div>
      <div class="card-buttons"><button type="button" class="edit button">edit</button><button class="delete button">delete</button></div>`;
      card.append(post);
      this.users.forEach(userEl => {
        if (userEl.id === el.userId) {
          const user = document.createElement("div");
          user.classList.add("user");
          user.innerHTML = `<span class="name">${userEl.name}</span>
          <span class="email">${userEl.email}</span>`;
          card.prepend(user);
          card.querySelector(".edit").addEventListener("click", this.editPost);
          card
            .querySelector(".delete")
            .addEventListener("click", this.deletePost);
        }
      });

      container.append(card);
    });
  }
  editPost(event) {
    let titleToEdit = event.target.parentElement.parentElement.querySelector(
      ".title"
    ).innerHTML;
    let textToEdit = event.target.parentElement.parentElement.querySelector(
      ".text"
    ).innerHTML;
    let post = event.target.parentElement.parentElement;
    post.querySelector(".title").innerHTML = "";
    post.querySelector(".text").innerHTML = "";

    const titleArea = document.createElement("textarea");
    titleArea.classList.add("edit-title");

    const textArea = document.createElement("textarea");
    textArea.classList.add("edit-text");
    titleArea.innerHTML = titleToEdit;
    textArea.innerHTML = textToEdit;
    titleToEdit = "";
    textToEdit = "";
    post.append(titleArea);
    post.append(textArea);
    const doneEdit = document.createElement("div");
    doneEdit.innerHTML = `<button type="button" class="done-edit">done</button>`;
    post.append(doneEdit);
    titleArea.addEventListener("input", function changeTitle(e) {
      titleArea.innerHTML = e.target.value;
    });
    textArea.addEventListener("input", function changeText(e) {
      textArea.innerHTML = e.target.value;
    });

    post
      .querySelector(".done-edit")
      .addEventListener("click", function save(event) {
        let id = post.parentElement.dataset.id;
        let user = post.parentElement.dataset.user;
        post.querySelector(".text").innerHTML = textArea.innerHTML;
        post.querySelector(".title").innerHTML = titleArea.innerHTML;
        let title = post.querySelector(".title").innerHTML;
        let body = post.querySelector(".text").innerHTML;

        post.removeChild(textArea);
        post.removeChild(titleArea);

        post.removeChild(doneEdit);
        // console.log(el.id)
        let url = "https://jsonplaceholder.typicode.com/posts/" + `${user}`;
        fetch(url, {
          method: "PUT",
          body: JSON.stringify({
            id: `${id}`,
            title: `${title}`,
            body: `${body}`,
            userId: `${user}`
          }),
          headers: {
            "Content-type": "application/json; charset=UTF-8"
          }
        })
          .then(response => response.json())
          .then(json => console.log(json));
      });
  }

  addPost(event) {
    let modal = document.getElementById("myModal");
    modal.querySelector(".new-title").value = "";
    modal.querySelector(".new-text").value = "";
    modal.style.display = "block";

    let closeModal = document.getElementsByClassName("close")[0];
    closeModal.addEventListener("click", function() {
      modal.style.display = "none";
    });

    let newTitle = modal.querySelector(".new-title");
    let newText = modal.querySelector(".new-text");
    newTitle.addEventListener("input", function crTitle(e) {
      newTitle.innerHTML = e.target.value;
    });
    newText.addEventListener("input", function crText(e) {
      newText.innerHTML = e.target.value;
    });
    modal
      .querySelector(".create-post")
      .addEventListener("click", createPost.bind(this));
    function createPost() {
      const card = document.createElement("div");
      card.classList.add("card");
      card.setAttribute("data-id", this.counterOfNewPosts++);
      card.setAttribute("data-user", 1);

      const post = document.createElement("div");
      post.classList.add("post");

      post.innerHTML = `<div class="title">${newTitle.innerHTML}</div>
      <div class="text">${newText.innerHTML}
      </div>
      <div class="card-buttons"><button type="button" class="edit button">edit</button><button class="delete button">delete</button></div>`;
      card.append(post);

      // fetch("https://jsonplaceholder.typicode.com/users")
      //   .then(response => response.json())
      //   .then(json => {
      //     users = json;
      // console.log(this)
      const user = document.createElement("div");
      user.classList.add("user");
      // console.log(this.users[0].name)

      user.innerHTML = `<span class="name">${this.users[0].name}</span>
          <span class="email">${this.users[0].email}</span>`;
      card.prepend(user);
      card.querySelector(".edit").addEventListener("click", this.editPost);
      card.querySelector(".delete").addEventListener("click", this.deletePost);
      let container = document.querySelector(".container");
      container.prepend(card);
      let addBtn = container.querySelector(".button-add-post");
      container.removeChild(addBtn);
      container.prepend(addBtn);
      modal.style.display = "none";

      fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
          title: `${newTitle.innerHTML}`,
          body: `${newText.innerHTML}`,
          userId: 1
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      })
        .then(response => response.json())
        .then(json => console.log(json));
      // });
    }
  }
  deletePost(event) {
    let result = confirm("Want to delete?");
    if (result) {
      let card = event.target.parentElement.parentElement.parentElement;
      let id = card.dataset.id;
      card.parentElement.removeChild(card);
      // let user = card.dataset.user;
      // let title = event.target.parentElement.parentElement.querySelector(
      //   ".title"
      // ).innerHTML;
      // let body = event.target.parentElement.parentElement.querySelector(".text")
      //   .innerHTML;
      // console.log(id, user, title, body);
      let url = "https://jsonplaceholder.typicode.com/posts/" + `${id}`;
      fetch(url, {
        method: "DELETE"
      });
    }
  }
}
